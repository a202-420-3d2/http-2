const express = require('express');

const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/public/index.html`);
});

app.get('/image.jpg', (req, res) => {
  res.sendFile(`${__dirname}/public/image.jpg`);
});

app.listen(port, () => {
  console.log(`Cette application web peut maintenant recevoir des requêtes: http://localhost:${port}`);
});
